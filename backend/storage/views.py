from django.shortcuts import render

from rest_framework import viewsets

from .models import CatModel, CatProject
from .serializers import CatModelSerializer, CatProjectSerializer

class CatModelViewSet(viewsets.ModelViewSet):
	queryset = CatModel.objects.all().order_by('-creation_time')
	serializer_class = CatModelSerializer
	
	def perform_create(self, serializer):
		serializer.save(creator=self.request.user)

	def perform_destroy(self, catmodel):
		catmodel.delete()

class CatProjectViewSet(viewsets.ModelViewSet):
	queryset = CatProject.objects.all().order_by('-name')
	serializer_class = CatProjectSerializer

	def perform_create(self, serializer):
		serializer.save(creator=self.request.user)

	def perform_destroy(self, catproject):
		catproject.delete()

