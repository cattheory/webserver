from rest_framework import serializers

import os

from django.conf import settings

from .statebuilder import ModelState
from .models import CatProject, CatModel

class CatModelSerializer(serializers.ModelSerializer):
    creator = serializers.ReadOnlyField(source='creator.username')
    viewmodel = serializers.SerializerMethodField()
    class Meta:
        model = CatModel
        fields = ('id', 'name', 'body', 'creation_time', 'project_name', 'creator', 'viewmodel')
        read_only_fields = ('viewmodel',)

    def get_viewmodel(self, obj):
        state = ModelState()
        root = settings.BASE_DIR
        bodyfile = os.path.normpath(root + "/" + str(obj.body))
        try:
            state.load(bodyfile)
            return state.view_model
        except:
            return None

class CatProjectSerializer(serializers.ModelSerializer):
	creator = serializers.ReadOnlyField(source='creator.username')
	class Meta:
		model = CatProject
		fields = ('id', 'name', 'creator')


