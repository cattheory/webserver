from django import forms

from .models import CatProject, CatModel

class CatProjectForm(forms.ModelForm):

    class Meta:
        model = CatProject
        fields = ('name', 'body', 'project_name')

class CatModelForm(forms.ModelForm):

    class Meta:
        model = CatProject
        fields = ('name', )

