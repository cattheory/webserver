from rest_framework import routers

from .views import CatModelViewSet, CatProjectViewSet

# Создаем router и регистрируем наш ViewSet
router = routers.DefaultRouter()
router.register('catmodels', CatModelViewSet)
router.register('catprojects', CatProjectViewSet)

# URLs настраиваются автоматически роутером
urlpatterns = router.urls

