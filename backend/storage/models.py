from django.db import models
from django.conf import settings
from django.core.exceptions import PermissionDenied

class CatModel(models.Model):
	creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	body = models.FileField(upload_to="uploads/")
	creation_time = models.DateTimeField(auto_now_add=True)
	project_name = models.ForeignKey('CatProject', on_delete=models.CASCADE)
	
	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.project_name.creator == self.creator:
			models.Model.save(self, *args, **kwargs)
		else:
			raise PermissionDenied("You can add files only to your project")

class CatProject(models.Model):
	name = models.CharField(max_length=255)
	creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	def __str__(self):
		return self.name

