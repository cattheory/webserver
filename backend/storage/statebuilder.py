import xml
import xml.etree.ElementTree
import zipfile
import sys

class ModelState(object):

    def __read_object(self, element, map):
        id = element.attrib['id']
        obj = {"id" : id}
        map[id] = obj
        return obj

    def __read_morphism(self, element, map):
        id = element.attrib['id']
        points = list(element)
        src = points[0]
        srcid = src.attrib['id']
        
        dst = points[1]
        dstid = dst.attrib['id']

        m = {"id" : id, "source" : srcid, "target" : dstid}
        map[id] = m
        return m

    def __read_composition(self, element):
        composition = []
        for el in list(element):
            if el.tag != "reference":
                raise Exception("Wrong tag: %s" % el.tag)
            mid = el.attrib['id']
            composition.append(mid)
        return composition

    def __read_compositions(self, element):
        compositions = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Composition":
                c = self.__read_composition(el)
                compositions.append(c)
            else:
                raise Exception("Unknown element %s" % etype)
        return compositions

    def __read_morphism_composition(self, element, map):
        id = element.attrib['id']
        composition = {'id': id, 'morphisms': []}
        for el in list(element):
            if el.tag != "reference":
                raise Exception("Wrong tag: %s" % el.tag)
            mid = el.attrib['id']
            composition['morphisms'].append(mid)
        map[id] = composition
        return composition

    def __read_morphism_compositions(self, element, map):
        compositions = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "MorphismComposition":
                c = self.__read_morphism_composition(el, map)
                compositions.append(c)
            else:
                raise Exception("Unknown element %s" % etype)
        return compositions


    def __read_diagram(self, element, map):
        id = element.attrib['id']
        diag = {'id': id, 'compositions': []}
        
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Compositions":
                diag['compositions'] += self.__read_compositions(el)
            else:
                raise Exception("Unknown element %s" % etype)

        map[id] = diag
        return diag

    def __read_objects(self, element, map):
        objects = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Object":
                obj = self.__read_object(el, map)
                objects.append(obj)
            else:
                raise Exception("Unknown element %s" % etype)
        return objects

    def __read_morphisms(self, element, map):
        morphisms = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Morphism":
                m = self.__read_morphism(el, map)
                morphisms.append(m)
            else:
                raise Exception("Unknown element %s" % etype)
        return morphisms    

    def __read_diagrams(self, element, map):
        diagrams = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Diagram":
                m = self.__read_diagram(el, map)
                diagrams.append(m)
            else:
                raise Exception("Unknown element %s" % etype)
        return diagrams

    def __load_attribute(self, attr, map):
        id = attr.attrib['id']
        name = attr.attrib['name']
        if name == "Reference":
            value = attr.attrib['value']
            type = attr.attrib['type']
            map[id]["reference"] = value
            map[id]["type"] = type
        elif name == "IdMorphism":
            map[id]["id_morphism"] = True
        elif name == "Content":
            value = attr.attrib['value']
            type = attr.attrib['type']
            map[id]["content"] = value
            map[id]["type"] = type
        elif name == "Color":
            #print("set color for", map[id])
            value = attr.attrib['value']
            map[id]["color"] = value
        elif name == "Position":
            x = attr.attrib['x'].replace(",", ".")
            y = attr.attrib['y'].replace(",", ".")
            map[id]["position"] = {"x": float(x), "y": float(y)}
        elif name == "Expansion":
            value = attr.attrib['value']
            map[id]["expanded"] = bool(value)
        elif name == "Product":
            map[id]["product"] = True
        elif name == "CoProduct":
            map[id]["coproduct"] = True
        elif name == "Component":
            value = attr.attrib['value']
            if not "components" in map[id].keys():
                map[id]["components"] = []
            map[id]["components"].append(value)
        else:
            raise Exception("Unknown attribute: %s" % name)

    def __load_attributes(self, element, map):
        for attr in list(element):
            self.__load_attribute(attr, map)

    def __read_category(self, element, map):
        id = element.attrib['id']
        name = element.attrib['name']
        category = {'id': id, 'name': name}
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Objects":
                category["objects"] = self.__read_objects(el, map)
            elif etype == "Morphisms":
                category["morphisms"] = self.__read_morphisms(el, map)
            elif etype == "Diagrams":
                category["diagrams"] = self.__read_diagrams(el, map)
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Attributes":
                self.__load_attributes(el, map)
        map[id] = category
        return category

    def __read_categories(self, element, map):
        categories = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Category":
                cat = self.__read_category(el, map)
                categories.append(cat)
            else:
                raise Exception("Unknown element %s" % etype)
        return categories

    def __read_mapping_item(self, element, map):
        mapping = {}
        source = list(element)[0]
        if source.tag != "reference":
            raise Exception("Wrong tag: %s" % source.tag)
        mid = source.attrib['id']
        mapping["source"] = mid
        target = list(element)[1]
        if target.tag != "reference":
            raise Exception("Wrong tag: %s" % target.tag)
        mid = target.attrib['id']
        mapping["target"] = mid
        return mapping

    def __read_mapping(self, element, map):
        mapping = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "ObjectMapping":
                omap = self.__read_mapping_item(el, map)
                mapping.append(omap)
            elif etype == "MorphismMapping":
                omap = self.__read_mapping_item(el, map)
                mapping.append(omap)
            elif etype == "DiagramMapping":
                omap = self.__read_mapping_item(el, map)
                mapping.append(omap)
            else:
                raise Exception("Unknown element %s" % etype)
        return mapping

    def __read_mappings(self, element, map):
        mappings = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Mapping":
                cat = self.__read_mapping(el, map)
                mappings.append(cat)
            else:
                raise Exception("Unknown element %s" % etype)
        return mappings

    def __read_nt(self, element, map):
        return {}

    def __read_nts(self, element, map):
        nts = []
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "NaturalTransformation":
                cat = self.__read_nt(el, map)
                nts.append(cat)
            else:
                raise Exception("Unknown element %s" % etype)
        return nts


    def __read_category_system(self, element, map):
        category_system = {}
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Categories":
                category_system["categories"] = self.__read_categories(el, map)
            elif etype == "Mappings":
                category_system["mappings"] = self.__read_mappings(el, map)
            elif etype == "MorphismCompositions":
                category_system["morphism_compositions"] = self.__read_morphism_compositions(el, map)
            elif etype == "NaturalTransformations":
                category_system["natural_transformations"] = self.__read_nts(el, map)
            else:
                raise Exception("Unknown element %s" % etype)
        return category_system

    def __load_state(self, element, map):
        for el in list(element):
            if el.tag != "container":
                raise Exception("Wrong tag: %s" % el.tag)
            etype = el.attrib['type']
            if etype == "Categories":
                self.__load_attributes(el, map)
            else:
                raise Exception("Unknown element %s" % etype)
        for cat in self.category_system["categories"]:
            try:
                color = cat["color"]
            except:
                color = "black"
            for obj in cat["objects"]:
                obj["color"] = color

    def __get_content(self, id, map):
        if (not "reference" in map[id].keys()) or (map[id]["reference"] == ""):
            try:
                return map[id]["content"]
            except:
                return ""
        if ("content" in map[id].keys()) and (map[id]["content"] != ""):
            return map[id]["content"]
        return self.__get_content(map[id]["reference"], map)

    def __build_view_model(self, map):
        state = {
            "models" : [],
            "singles" : [],
            "products" : [],
            "coproducts" : [],
            "morphisms" : [],
            "links" : [],
            "diagrams" : [],
            "mappings" : [],
            "morphism_compositions" : [],
            "natural_transfromations" : [],
        }
        for cat in self.category_system["categories"]:
            objs = []
            for obj in cat["objects"]:
                obj["is_component"] = False

            for obj in cat["objects"]:
                if "product" in obj.keys() and obj["product"] == True:
                    obj["object_type"] = "product"
                elif "coproduct" in obj.keys() and obj["coproduct"] == True:
                    obj["object_type"] = "coproduct"
                else:
                    obj["object_type"] = "single"
                
                if obj["object_type"] == "product":
                    components = obj["components"]
                    for c in components:
                        morphism = map[c]
                        target = map[morphism["target"]]
                        target["is_component"] = True
                if obj["object_type"] == "coproduct":
                    components = obj["components"]
                    for c in components:
                        morphism = map[c]
                        source = map[morphism["source"]]
                        source["is_component"] = True

            for obj in cat["objects"]:
                try:
                    if not obj["is_component"]:
                        ot = obj["object_type"]
                        if ot == "single":
                            om = {
                                "id" : obj["id"],
                                "x" : obj["position"]["x"],
                                "y" : obj["position"]["y"],
                                "color" : obj["color"],
                            }
                            try:
                                om["reference"] = obj["reference"]
                            except:
                                om["reference"] = ""
                            om["content"] = self.__get_content(obj["id"], self.map)
                            state["singles"].append(om)
                        elif ot == "product":
                            om = {
                                "id" : obj["id"],
                                "x" : obj["position"]["x"],
                                "y" : obj["position"]["y"],
                                "color" : obj["color"],
                                "components" : [],
                            }
                            for c in obj["components"]:
                                morphism = map[c]
                                target = map[morphism["target"]]
                                comp = {
                                    "id" : target["id"],    
                                }
                                try:
                                    rid = target["reference"]
                                    comp["reference"] = rid
                                except:
                                    comp["reference"] = ""
                                comp["content"] = self.__get_content(target["id"], self.map)
                                om["components"].append(comp)
                                objs.append(comp["id"])
                            state["products"].append(om)
                        elif ot == "coproduct":
                            om = {
                                "id" : obj["id"],
                                "x" : obj["position"]["x"],
                                "y" : obj["position"]["y"],
                                "color" : obj["color"],
                                "components" : [],
                            }
                            for c in obj["components"]:
                                morphism = map[c]
                                source = map[morphism["source"]]
                                comp = {
                                    "id" : source["id"],
                                }
                                try:
                                    comp["reference"] = source["reference"]
                                except:
                                    comp["reference"] = ""
                                comp["content"] = self.__get_content(source["id"], self.map)
                                om["components"].append(comp)
                                objs.append(comp["id"])
                            state["coproducts"].append(om)
                        objs.append(obj["id"])
                except:
                    pass

            ms = []
            for m in cat["morphisms"]:
                
                src = m["source"]
                dst = m["target"]

                if not src in objs or not dst in objs:
                    continue

                mm = {
                    "id" : m["id"],
                    "source" : src,
                    "target" : dst,
                    "type" : "morphism",
                }
                try:
                    mm["content"] = m["content"]
                except:
                    mm["content"] = ""

                try:
                    mm["identity"] = m["id_morphism"]
                except:
                    mm["identity"] = False
                print("Append morphism ", mm)
                state["morphisms"].append(mm)
                ms.append(m["id"])

            try:
                cm = {
                    "id" : cat["id"],
                    "name" : cat["name"],
                    "color" : cat["color"],
                    "objects" : objs,
                }
                state["models"].append(cm)
            except:
                continue

            for d in cat["diagrams"]:
                state["diagrams"].append(d)
                
        alls = state["singles"] + state["morphisms"]
        for pr in state["products"]:
            alls += pr["components"]
        for cpr in state["coproducts"]:
            alls += cpr["components"]
        for item in alls:
            if not "reference" in item.keys() or item["reference"] == "":
                continue
            ref = item["reference"]
            if not ref in map.keys():
                # TODO: display error
                continue
            id = item["id"]
            state["links"].append({
                "source" : id,
                "target" : ref,
                "type" : "reference",
            })

        return state

    def load(self, file):
        self.map = {}
        print("Loading file ", file)
        z = zipfile.ZipFile(file)
        print("Loading model")
        x = z.read('CategoryModel.xml')
        self.modelxml = xml.etree.ElementTree.fromstring(x)

        if self.modelxml.tag != "container":
            raise Exception("Wrong tag: %s" % self.modelxml.tag)
        if self.modelxml.attrib['type'] != 'CategorySystem':
            raise Exception("Wrong element %s" % (self.modelxml.attrib['type']))
        self.category_system = self.__read_category_system(self.modelxml, self.map)

        x = z.read('CategoryViewModel.xml')
        self.statexml = xml.etree.ElementTree.fromstring(x)
        if self.statexml.tag != "container":
            raise Exception("Wrong tag: %s" % self.modelxml.tag)
        if self.statexml.attrib['type'] != 'CategorySystemViewModel':
            raise Exception("Wrong element %s" % (self.modelxml.attrib['type']))
        self.__load_state(self.statexml, self.map)
        self.view_model = self.__build_view_model(self.map)

if __name__ == "__main__":
    file = sys.argv[1]
    state = ModelState()
    state.load(file)
    #print(state.view_model)