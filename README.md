# webServer

django + vue.js

https://app.moqups.com/GB60H1kTvB/view

## Frontend

Настройка окружения и запуск:

```bash
npm install -g @vue/cli

npm install --save axios

npm run serve
```

##Backend

Настройка окружения и запуск:

```python
pip3 install Django
pip3 install djangorestframework
pip3 install django-cors-headers
pip3 install django-currentuser
pip3 install django-rest-auth
pip3 install django-allauth

python3 manage.py makemigrations

python3 manage.py migrate

python3 manage.py runserver
```
