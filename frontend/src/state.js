export function logined(new_username, new_key)
{
    console.log("Logged in")
    localStorage.auth_key = new_key
    localStorage.username = new_username
    localStorage.auth = true
}

export function logouted()
{
    console.log("Logged out")
    localStorage.auth_key = null
    localStorage.username = null
    localStorage.auth = false
}

export var projects = {}

export function save_projects(items)
{
    projects = {}
    items.forEach(item => {
        projects[toString(item.id)] = {name: item.name, creator: item.creator}
    });
}

